package nl.utwente.di.temp;

public class Quoter {
    double calculateTemp(String tempInCalculus){
        int version_x = Integer.parseInt(tempInCalculus);
        double fahrenheit = 1.8* version_x + 32;
        return fahrenheit;
    }
}
