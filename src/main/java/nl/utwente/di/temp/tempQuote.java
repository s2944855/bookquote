package nl.utwente.di.temp;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


public class tempQuote extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private Quoter quoter;

    public void init() throws ServletException {
        quoter = new Quoter();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Temperature Value in Calculus:";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P> Calculus Degree: " +
                request.getParameter("calculus_degree") + "\n" +
                "  <P>Fahrenheit Degree: " +
                Double.toString(quoter.calculateTemp(request.getParameter("calculus_degree"))) +
                "</BODY></HTML>");
    }


}